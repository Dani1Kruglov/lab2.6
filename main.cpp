#include <iostream>


/*Вариант 1.
Подобрать структуру для хранения данных,
над которой преимущественно будут осуществляться операции удаления и дублирования.
Минимально – поиск.

//Тк поиск минимален => лучше всего подойдет односвязный список.
*/




struct T_List{
    T_List* next;
    int age;
};


void ADD(T_List* head, int age){
    T_List* p = new T_List;
    p->age = age;
    p->next = head->next;
    head->next = p;
}

void PRINT(T_List* head){
    T_List* p = head->next;
    while (p != nullptr)
    {
        std::cout << p->age << " ";
        p = p->next;
    }
}

void DELETE(T_List* head, int b){
    T_List* tmp;
    T_List* p = head;
    while (p->next != nullptr)
    {
        if (p->next->age == b)
        {
            tmp = p->next;
            p->next = p->next->next;
            delete tmp;
        }
        else
            p = p->next;
    }
}

void CLEAR(T_List* head){
    T_List* tmp;
    T_List* p = head->next;
    while (p != nullptr)
    {
        tmp = p;
        p = p->next;
        delete tmp;
    }
}


void DUPLICATE(T_List* head, int b){
    T_List* p = head->next;
    while (p != nullptr)
    {
        if (p->age == b)
        {
            T_List* q = new T_List;
            q->next = p->next;
            p->next = q;
            q->age = p->age;
            p = p->next;
        }
        p = p->next;
    }
}


void SWAPSORT(T_List* head){
    T_List* p = head->next;
    while (p->next->next != nullptr)
    {
        T_List* q = p->next;
        while (q->next != nullptr)
        {
            if (p->age > q->age)
                std::swap(p->age, q->age);
            q = q->next;
        }
        p = p->next;
    }
}

int main()

{
    T_List* head = new T_List;

    head->next = nullptr;

    std::cout << "Список: ";
    ADD(head, 100);
    ADD(head, 44);
    ADD(head, 1);
    ADD(head, 16);
    ADD(head, 138);
    PRINT(head);
    std::cout << std::endl;

    std::cout << "Список + удаление: ";
    DELETE(head,16);
    PRINT(head);
    std::cout << std::endl;

    std::cout << "Список + дублирование: ";
    DUPLICATE(head, 44);
    PRINT(head);
    std::cout << std::endl;

    CLEAR(head);
    delete head;
    return 0;
}





